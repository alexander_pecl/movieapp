package com.example.movieapp

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.movieapp.databinding.FragmentStartViewBinding

class StartViewFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {



        val binding = DataBindingUtil.inflate<FragmentStartViewBinding>(inflater, R.layout.fragment_start_view, container, false)

        val card1: MovieCard = MovieCard(getString(R.string.title), getString(R.string.creators), getString(R.string.actors))
        binding.card1 = card1

        val card2: MovieCard =  MovieCard(getString(R.string.title2), getString(R.string.creators2), getString(R.string.actors2))
        binding.card2 = card2

        val card3: MovieCard =  MovieCard(getString(R.string.title3), getString(R.string.creators3), getString(R.string.actors3))
        binding.card3 = card3

        binding.Movie1Bt.setOnClickListener { view: View ->
            view.findNavController().navigate(StartViewFragmentDirections.actionStartViewFragmentToMovie().setMovieSelected(1))
        }

        binding.Movie2Bt.setOnClickListener {
                view: View ->
            view.findNavController().navigate(StartViewFragmentDirections.actionStartViewFragmentToMovie().setMovieSelected(2))
        }

        binding.Movie3Bt.setOnClickListener { view: View ->
            view.findNavController().navigate(StartViewFragmentDirections.actionStartViewFragmentToMovie().setMovieSelected(3))
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item,requireView().findNavController()) ||super.onOptionsItemSelected(item)
    }
}
