package com.example.movieapp

data class Movie(var title:String = "No tiltle Yet",
                 var description:String = "No description yet",
                 var rating:Float = 0f,
                 var genre: String = "N/A",
                 var creator:String = "N/A",
                 var actors: String = "N/A")
