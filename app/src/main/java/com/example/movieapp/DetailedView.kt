package com.example.movieapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.movieapp.databinding.ActivityMainBinding
import com.example.movieapp.databinding.FragmentDetailedViewBinding
import com.example.movieapp.databinding.FragmentStartViewBindingImpl
import com.google.android.material.floatingactionbutton.FloatingActionButton

class DetailedView : Fragment() {
    private lateinit var binding: FragmentDetailedViewBinding
    private var movie : Movie = Movie()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detailed_view, container, false)
        //val binding = DataBindingUtil.setContentView<FragmentDetailedViewBinding>(requireActivity(), R.layout.fragment_detailed_view)

        val args = DetailedViewArgs.fromBundle(requireArguments())
        val img: ImageView = binding.imageView
        when(args.movieSelected) {

            1 -> {
                movie = Movie(getString(R.string.title),getString(R.string.movieDescription), 4.3f, getString(R.string.action_sci_fi), getString(R.string.creators), getString(R.string.actors))
                img.setImageResource(R.drawable.bild)
            }

            2 -> {
                movie = Movie(getString(R.string.title2),getString(R.string.movieDescription2), 4.3f, getString(R.string.genre2), getString(R.string.creators2), getString(R.string.actors2))
                img.setImageResource(R.drawable.blackwidow)
            }
            3 -> {
                movie = Movie(getString(R.string.title3),getString(R.string.movieDescription3), 4.3f, getString(R.string.genre3), getString(R.string.creators3), getString(R.string.actors3))
                img.setImageResource(R.drawable.blackclover)
            }
        }

        binding.movie = movie
        var button: FloatingActionButton = binding.floatingActionButton

        button.setOnClickListener{
            Toast.makeText(activity, "Movie Added to Watchlist", Toast.LENGTH_SHORT).show()
        }

        return binding.root
    }
}