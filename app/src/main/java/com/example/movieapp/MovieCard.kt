package com.example.movieapp

data class MovieCard(var title:String,
                     var creators:String,
                     var actors:String)